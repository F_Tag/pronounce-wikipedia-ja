#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import re
from glob import glob

import MeCab
from jaconv import kata2hira

PAD = 7
NO_PRONOUNCE = 'NO_PRONOUNCE'
NO_LABEL = 'NO_LABEL'
SAVE_DIRECTORY = os.path.join('..', 'data')


def txt2slist(text):
    """
    convert text to list of sentences
    """
    tmptxt = re.sub(' ?（.+?） ?', "", text)
    tmptxt = re.sub(' ?\(.+?\) ?', "", tmptxt)

    rlist = []
    for sent in tmptxt.splitlines()[1:]:
        if not sent == '':
            splsnt = sent.replace('。', '。__dummy__')  # dummy replace
            for tmpsnt in splsnt.split('__dummy__'):
                if not(tmpsnt == '' or tmpsnt == '。' or tmpsnt == ' '):
                    rlist.append(tmpsnt)

    return rlist


def id2dir(id):
    """
    get save directory from id
    """
    tmpid = id[::-1]
    lehgth = 3
    v = [tmpid[i: i + lehgth] for i in range(0, len(tmpid), lehgth)]
    return [name[::-1] for name in v[::-1]][:-1]


def main():
    pronounce = MeCab.Tagger(
        "-Ohatsuon -d /usr/local/lib/mecab/dic/mecab-ipadic-neologd")
    filelist = sorted(glob('../extracted/*/wiki_*', recursive=True))
    # filelist = filelist[:2] # for debug
    for filename in filelist:
        with open(os.path.join(filename)) as wikifile:
            for line in wikifile:
                linedict = json.loads(line)
                id = linedict.pop('id').zfill(PAD)
                text = linedict.pop('text')
                dirname = id2dir(id)
                sdir = os.path.join(SAVE_DIRECTORY, *dirname)
                os.makedirs(sdir, exist_ok=True)

                if '。' in text:
                    slist = txt2slist(text)
                    writelist = []
                    for snt in slist:
                        prn = kata2hira(''.join(pronounce.parse(snt).split()))
                        if re.search('[、。]{2,}', prn):
                            prn = NO_PRONOUNCE
                        if not re.search('^[ぁ-ん][ぁ-んー、。]+$', prn):
                            prn = NO_PRONOUNCE

                        writelist.append(snt + '\t' + prn + '\t' + NO_LABEL)

                    with open(os.path.join(sdir, id + '.csv'), 'w') as f:
                        for w in writelist:
                            f.write(w)
                            f.write('\n')


if __name__ == '__main__':
    main()
