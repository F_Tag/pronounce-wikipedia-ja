script_dir=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
cd $script_dir

WikiExtractor.py -o ../extracted ../dumped_jawiki/jawiki-latest-pages-articles.xml.bz2 --json
python make.py